import React from 'react'
import { getPost } from '../api'
import '../styles/style.css'

export default class Index extends React.Component {
  state = {
    data: null,
    loading: false,
  }

  componentDidMount() {
    this.setState({ loading: true })
    getPost().then((data) => this.setState({ data, loading: false }))
  }

  render() {
    const { data, loading } = this.state
    return (
      <main className="container">
        {loading && <h3>Loading...</h3>}
        {data && (
          <div className="post">
            <p>
              <strong>{data.author}</strong> | <em>{data.time}</em>
            </p>
            <p>{data.text}</p>
            <p>
              <button>load comments</button> <button>clear</button>
            </p>
          </div>
        )}
      </main>
    )
  }
}
