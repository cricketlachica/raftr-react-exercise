# raftr-react-exercise

In this exercise, you will need to create a reusable component for buttons, and also be able to load and list comments taken from an API.

### Getting started with the exercise

1. Clone this repo
2. Run `npm i` from the repo directory
3. Run `npm run dev` to start the project

