export const getPost = () => {
  return new Promise((resolve, reject) => {
    const postData = {
      author: 'Cricket La Chica',
      text:
        'Vegan activated charcoal XOXO seitan fam venmo raw denim DIY synth lumbersexual keytar. Mustache waistcoat cloud bread skateboard PBR&B letterpress. Bushwick next level occupy, austin shaman cred deep v ramps beard green juice. Austin af post-ironic pork belly listicle vape stumptown chartreuse.',
      time: 'a few days ago',
    }

    setTimeout(() => {
      resolve(postData)
    }, 1000)
  })
}

export const getComments = () => {
  return new Promise((resolve, reject) => {
    const commentsData = [
      {
        author: 'Helga Roberts',
        text:
          'Wayfarers organic tousled mumblecore adaptogen. Kale chips four dollar toast cliche, butcher lyft air plant swag. Slow-carb pop-up tilde heirloom portland pinterest listicle ennui shoreditch helvetica.',
        time: '2 days ago',
      },
      {
        author: 'Ryan Johnson',
        text:
          'Chambray literally YOLO roof party, intelligentsia ramps small batch vape. Palo santo swag celiac pitchfork gluten-free, deep v small batch hell of gastropub schlitz pork belly fashion axe vaporware pour-over prism. Cornhole organic chillwave, small batch gochujang post-ironic af wolf scenester four dollar toast single-origin coffee asymmetrical.',
        time: '2 days ago',
      },
      {
        author: 'Kelly Williams',
        text: 'VHS forage lyft iPhone sustainable beard cardigan irony helvetica art party franzen.',
        time: 'a day ago',
      },
      {
        author: 'Gary Santiago',
        text:
          'Selfies celiac 8-bit copper mug artisan, beard poke jianbing biodiesel meditation. Banjo direct trade kale chips, +1 artisan art party sartorial venmo blog brooklyn selvage meh tumblr four loko. Gentrify ugh snackwave kickstarter, selvage four dollar toast.',
        time: '6 hours ago',
      },
      {
        author: 'Bert Garrett',
        text: 'Narwhal art party shoreditch lyft scenester try-hard jean shorts deep v tumblr pork belly.',
        time: 'just now',
      },
    ]

    setTimeout(() => {
      resolve(commentsData)
    }, 1000)
  })
}
